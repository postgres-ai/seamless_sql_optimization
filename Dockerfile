FROM postgres:16

ENV POSTGRES_PASSWORD demo
ENV PGPASSWORD demo
ENV PGUSER postgres
ENV TERM xterm-256color

RUN apt-get update \
    && apt-get install -y vim htop procps sysstat dstat wget curl less pspg tmux jq git
RUN wget https://github.com/lesovsky/pgcenter/releases/download/v0.9.2/pgcenter_0.9.2_linux_amd64.tar.gz \
    && tar xzvf pgcenter* \
    && mv ./pgcenter /usr/bin
RUN wget https://github.com/agneum/plan-exporter/releases/download/v0.0.6/plan-exporter-0.0.6-linux-amd64.tar.gz \
    && tar -zxvf plan-exporter-0.0.6-linux-amd64.tar.gz \
    && cp plan-exporter-0.0.6-linux-amd64/plan-exporter /usr/local/bin/
#RUN apt-get install -y coreutils build-essential pandoc libxrender-dev libx11-dev golang \
#        libxext-dev libfontconfig1-dev libfreetype6-dev fontconfig \
#    && wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz \
#    && tar xvf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz \
#    && mv wkhtmltox/bin/wkhtmlto* /usr/local/bin \
#    && git clone https://gitlab.com/postgres-ai/postgres-checkup.git \
#    && cd postgres-checkup/pghrep \
#    && make install main
RUN git clone https://github.com/NikolayS/postgres_dba.git && true
RUN apt-get --purge remove -y git
#RUN wget https://dl.google.com/go/go1.13.9.linux-amd64.tar.gz \
#    && tar xf go1.13.9.linux-amd64.tar.gz \
#    && mv go /usr/local/go-1.13
RUN curl -sSL dblab.sh | bash


COPY . .

ENTRYPOINT ["./start.sh"]
