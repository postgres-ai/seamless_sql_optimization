#!/bin/bash
set -Eeo pipefail

echo "export GOROOT=/usr/local/go-1.13" >> ~/.bashrc
echo "export PATH=\$GOROOT/bin:/root/go/bin:\$PATH" >> ~/.bashrc

cat - <<CAT > ~/.psqlrc
\\set x '\\\\setenv PAGER less'
\\set xx '\\\\setenv PAGER \\'pspg -bX --no-mouse\\''
:xx

\\set dba '\\\\i /postgres_dba/start.psql'
CAT

dblab init --environment-id demo --url "https://demo.aws.postgres.ai" \
  --token "demo-token"

gosu postgres initdb --username="postgres" /var/lib/postgresql/data
cat - <<CONF >> /var/lib/postgresql/data/postgresql.conf
shared_preload_libraries = 'pg_stat_statements,auto_explain'
max_wal_size = '10GB'
autovacuum_vacuum_scale_factor = 0.01
autovacuum_analyze_scale_factor = 0.01
default_statistics_target = 1000
random_page_cost = 1.1
work_mem = '16MB'
shared_buffers = '200MB'
log_min_duration_statement = '1s'
CONF

echo "host all all 172.17.0.1/32 trust" >> /var/lib/postgresql/data/pg_hba.conf

gosu postgres pg_ctl -D /var/lib/postgresql/data -w start

psql -U postgres -c 'create database demo1'
pgbench -U postgres -i -s 100 demo1
psql -U postgres demo1 -f - <<SQL >/dev/null
create table extra_accounts(name text primary key, balance integer);
insert into extra_accounts select 'main', 0;
create function _1() returns trigger as \$\$begin update extra_accounts set balance = balance + NEW.abalance where name = 'main'; return NEW; end; \$\$ language plpgsql;
create trigger _1 before update on pgbench_accounts for each row execute procedure _1();
insert into pgbench_history select 0, 0, 0, 0, '2023-09-01', '' from generate_series(1, 10000000);
--create index i_pgbench_history_mtime on pgbench_history(mtime);
vacuum analyze;
create extension pg_stat_statements;
select pg_stat_statements_reset();
SQL

cat - <<SQL > t1.sql
\set aid random(1, 100000 * :scale)
\set bid random(1, 1 * :scale)
\set tid random(1, 10 * :scale)
\set delta random(-5000, 5000)
begin;
update pgbench_accounts set abalance = abalance + :delta where aid = :aid;
select abalance from pgbench_accounts where aid = :aid;
update pgbench_tellers set tbalance = tbalance + :delta where tid = :tid;
update pgbench_branches set bbalance = bbalance + :delta where bid = :bid;
insert into pgbench_history (tid, bid, aid, delta, mtime) values (:tid, :bid, :aid, :delta, current_timestamp);
end;
SQL

cat - <<SQL > t2.sql
select count(*) from pgbench_history where mtime::date = '2023-09-01';
SQL

cat - <<SQL > t3.sql
\set _day 1
select count(*) from pgbench_history where mtime::date = '2023-09-25';
SQL

cat - <<SQL > t4.sql
\set _day 26
select count(*) from pgbench_history where mtime::date = '2023-09-26';
SQL

echo
echo "*****   The workspace is ready!   *****"
echo

pgbench -U postgres -T 10800 -n -c8 -j8 -P 30 demo1 -f t1.sql@90 -f t2.sql@1 -f t3.sql@1 -f t4.sql@8

#sleep infinity

